import { Component } from "react";

class Service extends Component{


    getAllCharacter = async () => {
        const res = await fetch('https://rickandmortyapi.com/api/character')
        // const res = await fetch('https://rickandmortyapi.com/api/episode/20')
        if(!res.ok) throw new Error('Cant fetch url')

        const char = await res.json();
        console.log(char);
        return char.results.map(this._transformData);
        // return await char.result
    }

    _transformData = (char) => {
        return {
            gender: char.gender,
            id: char.id,
            image: char.image,
            location: char.location.name,
            name: char.name,
            origin: char.origin.name,
            species: char.species
        }
    }
}


export default Service