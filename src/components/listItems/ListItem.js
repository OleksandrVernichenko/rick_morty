import Item from "../item/Item";
import "./listItem.scss";

const ListItems = () => {
  return (
    <ul className="list">
      <Item />
    </ul>
  );
};

export default ListItems;
