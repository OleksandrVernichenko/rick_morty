import "./item.scss";
import Service from "../../service/Service";
import { useState, useEffect } from "react";

const Item = () => {
  const [charList, setCharList] = useState([]);
  const data = new Service();

  useEffect(() => {
    loadData();
  }, []);

  const loadData = () => {
    data.getAllCharacter().then((data) => setCharList(data));
  };

  return (
    <>
      {charList.slice(0,-2).map((item) => {
        const {name, id, image, gender, location, origin, species} = item;
        return (
          <li className="list-char" key={id}>
            <img src={image} alt="char" />
            <div className="list-char-name">{name}</div>
            <div className="list-char-species">{species}</div>
            <div className="list-char-gender">{gender}</div>
            <div className="list-char-location">{location}</div>
            <div className="list-char-origin">{origin}</div>
          </li>
        );
      })}
    </>
  );
};

export default Item;
