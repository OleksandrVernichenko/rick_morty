import './footer.scss';
import headerImg from "../../images/header.png";
import rick from '../../images/rick_morty.png';
import morty from '../../images/morty.png'

const Footer = () => {
    return(
        <footer className='footer'>
            <img src={headerImg} alt="footer logo" className='footer_logo' />
            <img src={morty} alt="morty" className='footer_morty' />
            <img src={rick} alt="rick_morty" className='footer_rick'/>
        </footer>
    )
}

export default Footer