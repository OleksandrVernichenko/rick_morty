import "./header.scss";
import headerImg from "../../images/header.png";
import rick from '../../images/rick_morty.png';
import head from '../../images/mouse.png'

const Header = () => {
  return (
    <header className="header">
      <img src={headerImg} alt="header" className="header-sign" />
      <img src={head} alt="head" className="header-head"/>
      <img src={rick} alt="rick_morty" className="header-rick"/>
    </header>
  );
};

export default Header;
