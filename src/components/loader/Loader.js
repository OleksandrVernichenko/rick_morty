import { useState, useEffect } from "react";
import "./loader.scss";
import image from "../../images/loader.png";

const Loader = () => {
  const [load, setLoad] = useState(true);
  const [angle, setAngle] = useState(1);
  const [imgAngle, setImgAngle] = useState(0);
  const [size, setSize] = useState(800);
  const loadStyle = {
    background: `conic-gradient(from ${angle}deg, #33FF01, #015D76)`,
  };
  const imgStyle = {
    transform: `rotate(${imgAngle}deg)`,
    height: `${size}px`,
  };

  useEffect(() => {
    if (load) {
      const timeout = setTimeout(() => {
        setAngle(angle + 1);
      }, 10);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [angle]);

  useEffect(() => {
    if (load) {
      const timeout = setTimeout(() => {
        setImgAngle(imgAngle - 1);
      }, 10);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [imgAngle]);

  useEffect(() => {
    if (load) {
      const timeout = setTimeout(() => {
        setSize(size - 1);
      }, 10);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [size]);

  useEffect(() => {
    console.log(size);
    size <= 1 ? setLoad(false) : setLoad(true);
  }, [size]);

  return (
    <>
      {load ? (
        <div className="loader" style={loadStyle}>
          <img src={image} alt="loader" style={imgStyle} />
        </div>
      ) : null}
    </>
  );
};

export default Loader;
