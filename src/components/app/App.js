import Loader from "../loader/Loader";
import Header from "../header/Header";
import ListItems from "../listItems/ListItem";
import Footer from "../footer/Footer";
const App = () => {
  return (
    <>
      <Loader />
      <Header />
      <main>
        <ListItems />
      </main>
      <Footer />
    </>
  );
};

export default App;
